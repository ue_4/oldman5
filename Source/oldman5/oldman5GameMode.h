// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "oldman5GameMode.generated.h"

UCLASS(minimalapi)
class Aoldman5GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Aoldman5GameMode();
};




// Copyright Epic Games, Inc. All Rights Reserved.

#include "oldman5GameMode.h"
#include "oldman5PlayerController.h"
#include "oldman5Character.h"
#include "UObject/ConstructorHelpers.h"

Aoldman5GameMode::Aoldman5GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = Aoldman5PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownPlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}
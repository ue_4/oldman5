// Copyright Epic Games, Inc. All Rights Reserved.

#include "oldman5.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, oldman5, "oldman5" );

DEFINE_LOG_CATEGORY(Logoldman5)
 